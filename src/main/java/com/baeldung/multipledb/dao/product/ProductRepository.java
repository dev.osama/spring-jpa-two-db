package com.baeldung.multipledb.dao.product;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.baeldung.multipledb.model.product.Product;
import org.springframework.stereotype.Repository;

@Repository()
//public interface ProductRepository extends PagingAndSortingRepository<Product, Integer> {
public interface ProductRepository extends JpaRepository<Product, Integer> {


    List<Product> findAllByPrice(double price, Pageable pageable);
}
