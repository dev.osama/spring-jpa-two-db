package com.baeldung.multipledb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collections;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.baeldung.multipledb.dao.product.ProductRepository;
import com.baeldung.multipledb.dao.user.PossessionRepository;
import com.baeldung.multipledb.dao.user.UserRepository;
import com.baeldung.multipledb.model.product.Product;
import com.baeldung.multipledb.model.user.Possession;
import com.baeldung.multipledb.model.user.User;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=MultipleDbApplication.class)
@EnableTransactionManagement
public class JpaMultipleDBIntegrationTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PossessionRepository possessionRepository;

    @Autowired
    private ProductRepository productRepository;

    // tests

    @Test
//    @Transactional("userTransactionManager")
    public void whenCreatingUser_thenCreated() {
        User user = new User();
        user.setName("osama");
        user.setEmail("osama@osama.com");
        user.setAge(20);
        user.setStatus(5);
        Possession p = new Possession("sample");
        p = possessionRepository.save(p);
        user = userRepository.save(user);
        final Optional<User> result = userRepository.findById(user.getId());
        assertTrue(result.isPresent());
    }

    @Test
//    @Transactional("userTransactionManager")
    public void whenCreatingUsersWithSameEmail_thenRollback() {
        User user1 = new User();
        user1.setName("saad");
        user1.setEmail("saad@saad.com");
        user1.setAge(20);
        user1.setStatus(5);
        user1 = userRepository.save(user1);
        assertTrue(userRepository.findById(user1.getId()).isPresent());

        User user2 = new User();
        user2.setName("ahmed");
        user2.setEmail("ahmed@ahmed.com");
        user2.setAge(10);
        user2.setStatus(5);
        user2 = userRepository.save(user2);
        assertTrue(userRepository.findById(user2.getId()).isPresent());
    }

    @Test
//    @Transactional("productTransactionManager")
    public void whenCreatingProduct_thenCreated() {
        Product product = new Product();
        product.setName("Book");
        product.setId(2);
        product.setPrice(20);
        product = productRepository.save(product);

        assertTrue(productRepository.findById(product.getId()).isPresent());
    }

}
